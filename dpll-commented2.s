    .text
    .file   "/cs/home/jdh9/.ccache/tmp/dpll.stdout.trenco.cs.st-andrews.ac.uk.30193.iB5geh.i"
    .globl  containsEmptyClause
    .align  16, 0x90
    .type   containsEmptyClause,@function
containsEmptyClause:                    # @containsEmptyClause, this is the function containsEmptyClause.
    .cfi_startproc
# BB#0:
    pushq   %rbp                        # %rbp is callee save so this subroutine must push it to the stack to save it so that it can be used and then restored before returning to the caller.
.Ltmp0:
    .cfi_def_cfa_offset 16
.Ltmp1:
    .cfi_offset %rbp, -16               # Appears to save the value of %rbp at 16 addresses before where it currently is [-16(%rbp)].
    movq    %rsp, %rbp                  # Copy contents of register %rsp (the stack pointer) to register %rbp.
.Ltmp2:
    .cfi_def_cfa_register %rbp
    subq    $32, %rsp                   # Subtract, q specifies 64 bit argument, subtract 32 from the stack pointer %rsp.
                                        # increases the size of the stack by 32 bytes, allocating space for this stack frame.
    movq    %rdi, -16(%rbp)             # Copy contents of register %rdi to the address 16 bytes before the address currently stored in %rbp (the stack frame pointer). %rdi is used to pass the first integer argument.
    movq    -16(%rbp), %rdi             # Copy contents of %rdi back to %rbp, this combined with the previous instruction is kind of pointless.

    # It looks like rdi actually held csPtr being passed in, it is then copied to -16(%rbp) for safekeeping, then the compiler realises it needs to pass csPtr to numClauses,
    # so it copies -16(%rbp) to %rdi again.

                                        # Something in the previous two instructions managed to pass csPtr to numClauses.
    callq   numClauses                  # Call function numClauses, pushes instruction pointer to stack and transfers control to the numClauses subroutine (in another file).
                                        # Calling numClauses() should set %eax to the return value of numClauses().
    movl    %eax, -20(%rbp)             # Copy doubleword in %eax (return value of numClauses) to 20 bytes before the address in %rbp.
                                        # So our variable num_clauses is in -20(%rbp).

    movl    $0, -24(%rbp)               # Zero the address 24 bytes before the address in %rbp. This is setting i to 0 in the for loop.
                                        # We can think of -24(%rbp) as holding the value of 'i'.

.LBB0_1:                                # =>This Inner Loop Header: Depth=1
                                        # We're in the for loop now.
    movl    -24(%rbp), %eax             # Copy -24(%rbp) to %eax, basically sets %eax to 'i' in the loop.
    cmpl    -20(%rbp), %eax             # Compare the value in -20(%rbp) and the value in %eax. This is the comparison i < num_clauses.
                                        # cmp instructions set values in the FLAGS register that can be picked up by 'jump' instructions.
    jge .LBB0_6                         # Jump if greater than or equal, looks in the FLAGS register to determine the case.
                                        # It will jump if i >= num_clauses since that signifies the end of the loop.

# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
# The following instructions are relevant to   if (csPtr->clauses[i].num_literals == 0)
# First we need the value csPtr->clauses[i].num_literals which requires array and struct member access.
    movslq  -24(%rbp), %rax             # Copy 'i' to %rax, we now have a copy of 'i' separate from the loop counter.
                                        # Instruction means "move long to quadword with sign extension" but I'm not sure what the significance of that is here.
    movq    -16(%rbp), %rcx             # Copy csPtr to %rcx, we now have a copy of our csPtr.
    addq    $8, %rcx                    # Add 8 to %rcx. Effectively makes %rcx point to the clauses array in csPtr since it comes after two integers in the struct definition.
    imulq   $24, %rax, %rax             # Multiply %rax by 24, multiplies our copy of 'i' by 24. 
                                        # The size of a clause struct is 24 bytes. So this gets us the position in bytes of the i'th element.
    addq    %rax, %rcx                  # Add i*24 to clauses array, this gets the i'th element of the clauses array and stores it in %rcx.
    cmpl    $0, 4(%rcx)                 # Compare 0 to the value in 4(%rcx), this is the if(csPtr->clauses[i].num_literals == 0).
                                        # %rcx holds the i'th element of clauses array. 4(%rcx) is the num_literals member. 
                                        # The first member of a clause struct is a char which is size 1 but due to memory alignment the next
                                        # member (num_literals) is at +4.
    jne .LBB0_4                         # Jump if not equal, this jumps to .LBB0_4 if clauses[i].num_literals != 0, continuing the loop.
# BB#3:
    # The following two lines are executed if csPtr->clauses[i].num_literals == 0 in containsEmptyClause and we want to return 1.
    movl    $1, -4(%rbp)                # Set the value of address in %rbp minus 4 bytes to 1. This is temporary as it is later copied to %eax so that it can be returned.
    jmp .LBB0_7                         # Jump to label .LBB0_7

.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
    jmp .LBB0_5                         # Jump to .LBB0_5

.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
    # Following three instructions increment the loop counter 'i'.
    movl    -24(%rbp), %eax             # Copy 'i' to %eax.
    addl    $1, %eax                    # Add one to the value in %eax, this increments the loop counter.
    movl    %eax, -24(%rbp)             # Copy the updated value back to -24(%rbp) so we are still using this address for 'i'.
    jmp .LBB0_1                         # Jump to LBB0_1, the next loop iteration.

.LBB0_6:
    movl    $0, -4(%rbp)                # Set -4(%rbp) to 0, this will then be copied to %eax since it is for the return value.

.LBB0_7:                                # This is the path that is executed if csPtr->clauses[i].num_literals == 0 in containsEmptyClause()
    movl    -4(%rbp), %eax              # Copy the temp value of 1 that was put in -4(%rbp) to %eax (the register for return values).       
    addq    $32, %rsp                   # Add 32 to the stack pointer, this counteracts the minus 32 that happened at the start of the function.
    popq    %rbp                        # Pops 64 bits from the stack and stores in %rbp. Restores %rbp to its value before calling this subroutine.
    # These previous two instructions seem to just restore the stack to how it was before calling this function.
    retq                                # "Return" by popping the return address from the stack and jumping there.
.Lfunc_end0:
    .size   containsEmptyClause, .Lfunc_end0-containsEmptyClause
    .cfi_endproc



    .globl  unitClause
    .align  16, 0x90
    .type   unitClause,@function
unitClause:                             # @unitClause
    .cfi_startproc
# BB#0:
    pushq   %rbp                        # push %rbp to the stack as we did at the start of the previous function, for the same reason.
.Ltmp3:
    .cfi_def_cfa_offset 16
.Ltmp4:
    .cfi_offset %rbp, -16
    movq    %rsp, %rbp                  # Copy the stack pointer to %rbp
.Ltmp5:
    .cfi_def_cfa_register %rbp
    # The following instructions up to and including .LBB1_1 are identical to the previous function since they 
    # deal with setting up the numClauses variable, calling numClauses with csPtr, and setting up the for loop.
    subq    $32, %rsp                   # Subtract 32 from the stack pointer. Increase size of stack by 32 bytes.
                                        # Allocate space for this stack frame.
    movq    %rdi, -16(%rbp)             # Copy the argument being passed in (csPtr) to -16(%rbp).
    movq    -16(%rbp), %rdi             # Copy the argument back to %rdi.
    callq   numClauses                  # Call numClauses with argument csPtr stored in %rdi.
    movl    %eax, -20(%rbp)             # Copy the return value in %eax to -20(%rbp)
    movl    $0, -24(%rbp)               # Zero out -24(%rbp), initialises 'i'.
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
    movl    -24(%rbp), %eax             # Copy 'i' to %eax.
    cmpl    -20(%rbp), %eax             # Compare our return value stored in -20(%rbp) to 'i'. This is checking the loop condition i < num_clauses.
    jge .LBB1_6                         # Jump if greater than or equal to .LBB1_6. This is the terminating condition i >= num_clauses.
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
    movslq  -24(%rbp), %rax             # Make a copy of 'i' in %rax.
    movq    -16(%rbp), %rcx             # Make a copy in csPtr argument in %rcx.
    addq    $8, %rcx                    # Add 8 to the csPtr and store in %rcx, we now point to the clauses array in csPtr.
    imulq   $24, %rax, %rax             # Multiply our copy of 'i' by 24.
                                        # Since size of clause struct is 24 bytes this gets us the i'th element position in bytes.
    addq    %rax, %rcx                  # Add our 'i*24' to the clauses array so we point at the correct clause.

    ### if (csPtr -> clauses[i].num_literals == 1)
    cmpl    $1, 4(%rcx)                 # Compare 1 to the value in 4(%rcx), this is the if(csPtr->clauses[i].num_literals >= 1).
                                        # %rcx holds the i'th element of clauses array. 4(%rcx) is the num_literals member. 
                                        # The first member of a clause struct is a char which is size 1 but due to memory alignment the next
                                        # member (num_literals) is at +4.
    jne .LBB1_4                         # Jump if not equal, we want to jump to the next loop iteration if the condition is not satisfied, 
                                        # else return csPtr->clauses[i].literals[0].
# BB#3:
    movslq  -24(%rbp), %rax             # Make a copy of 'i' in %rax again.
    movq    -16(%rbp), %rcx             # Make a copy of csPtr in %rcx again.
    addq    $8, %rcx                    # Add 8 to %rcx again so we point at clauses array.
    imulq   $24, %rax, %rax             # Multiply our copy of 'i' by 24 again.
    addq    %rax, %rcx                  # Add copy of 'i' to %rcx to point at clauses[i].
    movl    8(%rcx), %edx               # Copy 8(%rcx), which holds the literals array in clauses[i], to %edx.
    movl    %edx, -4(%rbp)              # Copy %edx to -4(%rbp)
    # The array acts as a pointer to its first element so we can think of this as being literals[0] in -4(%rbp).
    jmp .LBB1_7                         # Jump to .LBB1_7

# Moving to the next iteration of the for loop.
.LBB1_4:                                #   in Loop: Header=BB1_1 Depth=1
    jmp .LBB1_5                         # Jump to .LBB1_5

.LBB1_5:                                #   in Loop: Header=BB1_1 Depth=1
# Next 3 instructions increment 'i'.
    movl    -24(%rbp), %eax             # Copy 'i' to %eax.
    addl    $1, %eax                    # Add 1 to %eax.
    movl    %eax, -24(%rbp)             # Copy %eax back to -24(%rbp)
    jmp .LBB1_1                         # Jump to .LBB1_1, start of the next loop iteration.

.LBB1_6:
    movl    $0, -4(%rbp)                # After terminating the loop we want to return 0, copy 0 to -4(%rbp).

.LBB1_7:
    movl    -4(%rbp), %eax              # Copy -4(%rbp) to %eax so that we return literals[0], or 0 depending on where we came from.
    addq    $32, %rsp                   # Add 32 back onto the stack pointer.
    popq    %rbp                        # Pop %rbp from the stack again, restoring the stack.
    retq                                # "Return" by popping the return address from the stack and jumping there.
.Lfunc_end1:
    .size   unitClause, .Lfunc_end1-unitClause
    .cfi_endproc



    .globl  firstLiteral
    .align  16, 0x90
    .type   firstLiteral,@function
firstLiteral:                           # @firstLiteral
    .cfi_startproc
# BB#0:
    pushq   %rbp                        # push %rbp to the stack as we did at the start of the previous function, for the same reason.
.Ltmp6:
    .cfi_def_cfa_offset 16
.Ltmp7:
    .cfi_offset %rbp, -16
    movq    %rsp, %rbp                  # Copy the stack pointer to %rbp
.Ltmp8:
    .cfi_def_cfa_register %rbp

    # The following instructions up to and including .LBB2_1 are again identical to the previous function since they 
    # deal with setting up the numClauses variable, calling numClauses with csPtr, and setting up the for loop.
    subq    $32, %rsp                   # Subtract 32 from the stack pointer. Increase size of stack by 32 bytes.
                                        # Allocate space for this stack frame.
    movq    %rdi, -16(%rbp)             # Copy the argument being passed in (csPtr) to -16(%rbp).
    movq    -16(%rbp), %rdi             # Copy the argument back to %rdi.
    callq   numClauses                  # Call numClauses with argument csPtr stored in %rdi.
    movl    %eax, -20(%rbp)             # Copy the return value in %eax to -20(%rbp)
    movl    $0, -24(%rbp)               # Zero out -24(%rbp), initialises 'i'.

.LBB2_1:                                # =>This Inner Loop Header: Depth=1
    movl    -24(%rbp), %eax             # Copy 'i' to %eax.
    cmpl    -20(%rbp), %eax             # Compare our return value stored in -20(%rbp) to 'i'. This is checking the loop condition i < num_clauses.
    jge .LBB2_6                         # Jump if greater than or equal to, to .LBB2_6. Terminates loop when i >= num_clauses.
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
    movslq  -24(%rbp), %rax             # Make a copy of 'i' in %rax.
    movq    -16(%rbp), %rcx             # Make a copy of csPtr in %rcx.
    addq    $8, %rcx                    # Add 8 to %rcx, making it point at the clauses array in csPtr.
    imulq   $24, %rax, %rax             # Multiply our copy of 'i' by 24.
                                        # Since size of clause struct is 24 bytes this gets us the i'th element position in bytes.
    addq    %rax, %rcx                  # Add our 'i*24' to clauses array and store in %rcx to make %rcx point to clauses[i].
    cmpl    $0, 4(%rcx)                 # Compare 0 to 4(%rcx) which is 4 bytes ahead of the first thing in the struct at clauses[i].
                                        # As before this is the integer num_literals.
    jle .LBB2_4                         # Jump if less than or equal to. Jumps to the next loop iteration if 0 <= num_literals i.e. num_literals > 0.

# Get value of csPtr->clauses[i].literals[0] for returning.
# BB#3:
    movslq  -24(%rbp), %rax             # Make copy of 'i' in %rax
    movq    -16(%rbp), %rcx             # Make copy of csPtr in %rcx.
    addq    $8, %rcx                    # Add 8 to csPtr in %rcx, making it point at the clauses array in csPtr.
    imulq   $24, %rax, %rax             # Multiply our copy of 'i' by 24.
                                        # Since size of clause struct is 24 bytes this gets us the i'th element position in bytes.
    addq    %rax, %rcx                  # Add our 'i*24' to clauses array and store in %rcx to make %rcx point to clauses[i].
    movl    8(%rcx), %edx               # Copy the value 8 bytes ahead of the clauses[i] being pointed to by %rcx to %edx.
                                        # This is the literals array in clauses[i], which acts as a pointer to its first element.
                                        # So we have literals[0] in %edx.
    movl    %edx, -4(%rbp)              # Copy %edx to -4(%rbp), needed so that in both code paths -4(%rbp) can be copied to %eax for returning.
    jmp .LBB2_7                         # Jump to .LBB2_7, which tidies up and returns our value.

.LBB2_4:                                #   in Loop: Header=BB2_1 Depth=1
    jmp .LBB2_5

#Increment loop counter 'i'
.LBB2_5:                                #   in Loop: Header=BB2_1 Depth=1
    movl    -24(%rbp), %eax             # Copy loop counter 'i' to %eax.
    addl    $1, %eax                    # Add 1 to loop counter.
    movl    %eax, -24(%rbp)             # Copy loop counter back to -24(%rbp).
    jmp .LBB2_1                         # Jump to .LBB2_1, the next loop iteration.

.LBB2_6:
    movl    $0, -4(%rbp)                # Copy 0 to -4(%rbp) for returning, this is the zero case if the for loop terminates without returning literals[0].

.LBB2_7:
    movl    -4(%rbp), %eax              # Copy -4(%rbp) which is either 0 or csPtr->clauses[i].literls[0] to %eax so it can be returned.
    addq    $32, %rsp                   # Add 32 back onto the stack pointer.
    popq    %rbp                        # Pop 64 bits from the stack and store in %rbp, restoring the stack as it was before this function was called.
    retq                                # "Return" by popping the return address from the stack and jumping there.
.Lfunc_end2:
    .size   firstLiteral, .Lfunc_end2-firstLiteral
    .cfi_endproc



    .globl  dpll
    .align  16, 0x90
    .type   dpll,@function
dpll:                                   # @dpll
    .cfi_startproc
# BB#0:
    pushq   %rbp                        # push %rbp to the stack as we did at the start of the previous function, for the same reason.
.Ltmp9:
    .cfi_def_cfa_offset 16
.Ltmp10:
    .cfi_offset %rbp, -16
    movq    %rsp, %rbp                  # Copy the stack pointer to %rbp, the stack frame pointer.

.Ltmp11:
    .cfi_def_cfa_register %rbp
    subq    $9776, %rsp             # imm = 0x2630
                                    # Subtract 9776 from the stack pointer %rsp.
                                    # Increases the size of the stack by 9776 bytes. This seems to be 
                                    # allocating a bunch of memory on the stack for use in this function.
    leaq    16(%rbp), %rax          # Load effective address with quadword suffix.
                                    # Load the address stored in 16(%rbp) to %rax, the address, not the value of that address.
                                    # Based on later instructions, 16(%rbp) must point at the clauseset "cs" argument.
                                    # cs is a struct so it is passed by the stack rather than by a register like an int would be.
                                    # We use leaq to get the address of cs since that is the most useful way to handle this
                                    # and is what is passed into some of the functions called from this function.

# Increment node_count. node_count++
    movq    node_count, %rcx        # Copy node_count global variable to %rcx.
    addq    $1, %rcx                # Add one to %rcx.
    movq    %rcx, node_count        # Copy %rcx back to node_count.

# __asm__ ("nop;" );
    #APP
    nop                             # "Pointless assembler instruction" that Ian put in.

# if (cs.num_clauses == 0)
    #NO_APP
    cmpl    $0, 4(%rax)             # Compare 0 to the value 4 bytes after %rax. %rax holds the cs argument at this point
                                    # so this does "if (cs.num_clauses == 0)" since num_clauses is the second integer in the struct definition.
    movq    %rax, -7248(%rbp)       # 8-byte Spill: Copy address of cs argument to -7248(%rbp), into the memory we allocated.
    jne .LBB3_2                     # Jump if not equal to .LBB3_2. Jumps if cs.num_clauses != 0.

# BB#1: if the statement is true return 1.
    movb    $1, -1(%rbp)            # Copy 1 to one byte before the stack frame pointer. The place where we keep values to reuturn before actually returning.
    jmp .LBB3_12                    # Jump to .LBB3_12 so we can return 1.

# else if (containsEmptyClause(&cs))
.LBB3_2:
    movq    -7248(%rbp), %rdi       # 8-byte Reload
                                    # Copy what %rax was before we did the 8-byte spill to %rdi.
                                    # This gives us &cs in %rdi.
    callq   containsEmptyClause     # Call contains empty clause with the address of cs (&cs) as an argument.
    cmpl    $0, %eax                # Compare 0 to the return value of containsEmptyClause.
    je  .LBB3_4                     # Jump if equal to .LBB3_4. Jumps if containsEmptyClause returned zero.
                                    # This would move onto the else clause since the condition is if (containsEmptyClause(&cs)).

# BB#3:   if containsEmptyClause returns truthy value (non zero), increment failures and return zero.
    # Increment failures.
    movq    failures, %rax          # Copy global variable failures to %rax.
    addq    $1, %rax                # Add one to %rax.
    movq    %rax, failures          # Copy value of %rax back to failures.
    movb    $0, -1(%rbp)            # Copy 0 to -1(%rbp), this is the stack address we are using for the return value before copying to %eax.
    jmp .LBB3_12                    # Jump to .LBB3_12

# The top level else clause in dpll.
.LBB3_4:
    movq    -7248(%rbp), %rdi       # 8-byte Reload
                                    # Copy address of cs (&cs) to %rdi as its the first argument of unitClause(&cs).
                                    # For clarity, we leaq'd the cs struct into %rax, then copied %rax to -7248(%rbp).
    callq   unitClause              # Call unitClause with &cs as argument.
    movl    %eax, -8(%rbp)          # Copy return value to -8(%rbp), -8(%rbp) is therefore local variable unit. 

    # if (unit)
    cmpl    $0, -8(%rbp)            # Compare 'unit' to 0. [if (unit)]
    je  .LBB3_6                     # If unit == 0, jump to .LBB3_6 which is the else of [if(unit)].

# BB#5: This is where we return dpll(simplifyLiteral(cs, unit)) on line 90 of dpll.c
# Need to set up the two arguments in appropriate registers, then call simplifyLiteral and get the return value.
# The return value needs to be moved to an argument register for calling dpll.
# The return value of dpll needs to be returned.
#-- All of this is one line of C code --#
    ### cs is passed by value so we need to make a copy of the struct to pass in.
    leaq    -2416(%rbp), %rax       # Load effective address of -2416(%rbp) to %rax, this gives us an address of something.
                                    # Seems to be allocating space for the return value of the nested simplifyLiteral call.
                                    # But this address is loaded to %rdi later and the behaviour of that operation makes sense.
                                    # This one just ends up copying %rax back to the stack again and doesn't do anything useful with it.

    movl    -8(%rbp), %esi          # Copy 'unit' variable to %esi, the low 32 bits of %rsi, the second argument register.
                                    # The 'l' suffix, for doubleword (32 bits) means we're able to copy to %esi.
    movq    %rsp, %rcx              # Copy %rsp, the stack pointer to %rcx, this is so the destination of the struct copy
                                    # is the top of the stack, allowing simplifyLiteral to know where our copy is.
    movl    $301, %edx              # imm = 0x12D
                                    # Copy 301 to %edx, the low 32 bits of %rdx, the third argument register.
                                    # Don't think it being the third argument register is relevant here since we're only dealing with two arguments.
                                    # The 301 value eventually ends up in %rcx being used as a counter for rep instruction that makes
                                    # a copy of cs to pass into simplifyLiteral.
    movl    %edx, %edi              # Copy %edx to %edi, the low 32 bits of %rdi, the first argument register.

    ### Effectively swaps %rcx and %rdi, important because it gets the stack pointer into %rdi and lets us use %rcx for 301
    # It would have probably made more sense to do movq %rsp %rdi and movq $301 %rcx but maybe thats left for the optimised version.
    movq    %rcx, -7256(%rbp)       # 8-byte Spill : Copying %rcx to some stack location for safekeeping.
    movq    %rdi, %rcx              # Copy %rdi to %rcx, %rcx is count register used in the upcoming 'rep' instruction.
                                    # %rdi currently contains 301 in its low 32 bits (%edi).
    movq    -7256(%rbp), %rdi       # 8-byte Reload : Copying that stack location to %rdi
                                    # %rdi now holds the stack pointer, which we want for our copy destination.
    ###

    movq    -7248(%rbp), %r8        # 8-byte Reload : Copy  address of cs (&cs) to %r8.
    movl    %esi, -7260(%rbp)       # 4-byte Spill  : Copy %esi to some stack location. %esi still holding the 'unit' variable.
    movq    %r8, %rsi               # Copy value of %r8 to %rsi. &cs now in %rsi.

    # This actually makes a copy of the cs struct.
    rep;movsq                       # rep repeats the next instruction by the number of times in the %rcx register
                                    # So this does movsq 301 times.
                                    # movsq moves qword from %rsi or %esi to %rdi or %edi.
                                    # Means "move data from string to string".

                                    # %rsi is holding address of cs, so source is cs.
                                    # destination is %rdi, currently the stack pointer, which is 
                                    # overwritten in next instruction, but that's ok because 
                                    # we're passing cs to simplifyLiteral by the stack,
                                    # and it knows that cs should be at the end of the stack.
                                    
                                    # movsq decrements by 8 bytes each time, "points" at different address each time.
                                    # This makes a copy of cs to pass by value into simplifyLiteral.
                                    # 301 * 8 = 2408 bytes = sizeof(clauseset)

    # So we've now made a copy of the cs struct and it is located at the top of the stack.

    leaq    -2416(%rbp), %rdi       # Copy the address of the space for simplifyLiteral's return value into %rdi.
                                    # simplifyLiteral will know to write its return value here.
                                    # Address is passed as a "secret argument".
    movl    -7260(%rbp), %esi       # 4-byte Reload : Gets us back the 'unit' variable from the stack, copies it into %esi so it can be
                                    # 2nd argument to simplifyLiteral.
    movq    %rax, -7272(%rbp)       # 8-byte Spill : %rax still holds the address of the space for the return value.
                                    # This seems to just be saving it for later/%rax needed for something else so save its value in the meantime.
    callq   simplifyLiteral         # Call simplifyLiteral(cs, unit), the cs argument will be found at the top of the stack.

    ### The return value of simplifyLiteral is a clauseset, which will then be passed by value to dpll in the nested call.
    # So we need to do all that struct copying stuff again...
    leaq    -2416(%rbp), %rax       # Another possibly pointless load to %rax, probably disappears with optimisations.

    movq    %rsp, %rcx              # Copy stack pointer to %rcx
    movl    $301, %edx              # imm = 0x12D : Copy 301 to %edx, need to copy 301 * 8 bytes to copy clauseset struct.
    movl    %edx, %edi              # Copy %edx to %edi, this is all very roundabout and could have been condensed to movq 301 %rcx
    leaq    -2416(%rbp), %rsi       # Load the address of return value of simplifyLiteral to use as source for struct copy.
    movq    %rcx, -7280(%rbp)       # 8-byte Spill : Save %rcx to stack.
    movq    %rdi, %rcx              # Finally we get 301 into %rcx.
    movq    -7280(%rbp), %rdi       # 8-byte Reload : Reload old value of %rcx to %rdi, getting the stack pointer in %rdi.
    rep;movsq                       # Do the copy.
    movq    %rax, -7288(%rbp)       # 8-byte Spill : Copy that address we put in %rax to the stack.
    callq   dpll                    # Call dpll, the clauseset argument will be found at the top of the stack.
    movb    %al, -1(%rbp)           # Copy %al, the low 8 bits of %rax, to the stack.
                                    # -1(%rbp) is the stack location we are using for the return value before we jump to the 
                                    # place where we actually do the return, as seen in other code branches in this function.
    jmp .LBB3_12                    # Jump to label to do the return.
#-- All of that was one line of C code. --#

# else clause of [if (unit)]
.LBB3_6:
    movq    -7248(%rbp), %rdi       # 8-byte Reload : Copy our pointer to cs to the first argument register.
    callq   firstLiteral            # Call firstLiteral with &cs as argument.
    movl    %eax, -2420(%rbp)       # Copy return value from %eax to -2420(%rbp), -2420(%rbp) now holds the
                                    # local variable 'literal'.

    cmpl    $0, -2420(%rbp)         # Compare 0 to -2420(%rbp), this is actually calling assert(literal != 0)
                                    # First literal just returns an integer.
    je  .LBB3_8                     # Jump if equal to zero, meaning no clauses in the clauseset have any literals.
                                    # i.e. the assertion fails.
# BB#7:
    jmp .LBB3_9                     # If the assertion passes, jump to .LBB3_9, and continue the function.

# assert(literal != 0), this block is for when the assertion fails, assert has actually already been
# called using the [cmpl $0 -2420(%rbp)] in previous block.
.LBB3_8:
    movabsq $.L.str, %rdi           # Copy the defined string literal at label .L.str which in this case is "literal != 0"
                                    # to %rdi. It will be passed as an argument to __assert_fail (1st arg is the assertion).
    movabsq $.L.str.1, %rsi         # Copy the string literal at .L.str.1 "dpll.c" to %rsi so that it can be the second
                                    # argument to __assert_fail (second argument is the filename).
    movl    $95, %edx               # Copy immediate value 95 to %edx, the low 32 bits of %rdx, the 3rd argument register.
                                    # The third argument of __assert_fail is the line the assert was called on.
    movabsq $.L__PRETTY_FUNCTION__.dpll, %rcx   # Finally copy string from .L__PRETTY_FUNCTION__.dpll to %rcx
                                                # the 4th arg register. 4th argument is the function and the string in this case was
                                                # "char dpll(clauseset)"
    callq   __assert_fail           # Call __assert_fail with the 4 arguments we set up.

# Continuing the dpll function assuming the call to assert passed.
# This would be starting the line     char leftResult = dpll(simplifyLiteral(cs,literal))
# So we need to do another struct copying operation. 
# This will be very similar to the previous nested dpll(simplifyLiteral()) call except we do other things
# before returning.
.LBB3_9:
    leaq    -4832(%rbp), %rax       # Copy address of value at -4832(%rbp) to %rax.
                                    # POSSIBLY POINTLESS as discussed before.

    movl    -2420(%rbp), %esi       # Copy local variable "literal" to low 32 bits of 2nd argument register %esi.
    movq    %rsp, %rcx              # Copy stack pointer to %rcx, we want our struct copy at top of stack.

    # The roundabout way of getting stack pointer into %rdi and 301 into %rcx.
    movl    $301, %edx              # imm = 0x12D : copy immediate value 301 to %edx.
    movl    %edx, %edi              # Copy 301 to %edi.
    movq    %rcx, -7296(%rbp)       # 8-byte Spill : Copy %rcx, currently holding the value of the stack pointer, to a place on the stack.
    movq    %rdi, %rcx              # Copy our first argument register, currently holding 301, to %rcx.
    movq    -7296(%rbp), %rdi       # 8-byte Reload : Copy old value of %rcx (stack pointer value) to %rdi.
    movq    -7248(%rbp), %r8        # 8-byte Reload : Copy address of cs struct to %r8.
    movl    %esi, -7300(%rbp)       # 4-byte Spill : Copy local variable 'literal' to stack.
    movq    %r8, %rsi               # Copy address of cs struct to %rsi to use as source for the copy operation.

    rep;movsq                       # Copy the struct

    leaq    -4832(%rbp), %rdi       # Assign a place on the stack to hold the struct returned by simplifyLiteral, copy it to %rdi.
                                    # Address is passed as a "secret argument".
    movl    -7300(%rbp), %esi       # 4-byte Reload : Retrieve local variable 'literal' from stack. Put in 2nd argument reg.
    movq    %rax, -7312(%rbp)       # 8-byte Spill : Copy %rax to stack, %rax was holding location for returned struct from
                                    # simplifyLiteral (pointlessly).
    callq   simplifyLiteral         # Call simplifyLiteral(cs, literal).
                                    # The struct being returned should be at -4832(%rbp).
    leaq    -4832(%rbp), %rax       # Copy the address of the returned struct to %rax.

    # Again, the roundabout way of getting stack pointer into %rdi and 301 into %rcx.
    movq    %rsp, %rcx              # Copy stack pointer to %rcx.
    movl    $301, %edx              # imm = 0x12D : Copy 301 to %edx
    movl    %edx, %edi              # Copy 301 to %edi.
    leaq    -4832(%rbp), %rsi       # Copy address of our return value to use as source for struct copy.
                                    # Want to pass return value from simplifyLiteral to dpll().

    # Swap %rcx and %rdi
    movq    %rcx, -7320(%rbp)       # 8-byte Spill : Copy %rcx to stack
    movq    %rdi, %rcx              # Copy %rdi to %rcx
    movq    -7320(%rbp), %rdi       # 8-byte Reload : Copy old value of %rcx to %rdi.

    rep;movsq                       # Do the struct copy. Copy of return value of simplifyLiteral now at top of stack.

    movq    %rax, -7328(%rbp)       # 8-byte Spill : Copy %rax to stack.
                                    # Not sure why, seems like for safekeeping in case %rax is needed for something else.
                                    # Probably more pointlessness.

    callq   dpll                    # Call dpll with copy of return value of simplifyLiteral as argument.
    movb    %al, -2421(%rbp)        # Copy %al, low 8 bits of %rax i.e. return register, to stack.
                                    # Copying the character returned by dpll to the stack i.e. saving it as
                                    # local variable leftResult.

### if (leftResult)
    cmpb    $0, -2421(%rbp)         # Compare 0 to leftResult.
    je  .LBB3_11                    # Jump if leftResult == 0, this jumps to the else clause.

# BB#10:
### Here if leftResult is truthy i.e. != 0
    movb    -2421(%rbp), %al        # Copy leftResult to return register, fits in low 8 bits because its just a char.
    movb    %al, -1(%rbp)           # Copy %al to the stack location we're using to hold return values before we return.
    jmp .LBB3_12                    # Jump to label to do the return and clean up.

### else clause of if(leftResult)
### This is another one of those nested calls, need to do all that struct copying stuff again.
### Only difference is -literal as an argument.
.LBB3_11:
    leaq    -7240(%rbp), %rax       # Another possibly pointless load to %rax.
    xorl    %ecx, %ecx              # xor something with itself, zeroes out %ecx.
    subl    -2420(%rbp), %ecx       # Subtract local variable 'literal' from %ecx then store in %ecx.
                                    # This negates the variable which we wanted for passing it in.

    # Another one of those roundabout stack pointer into %rdi, 301 into %rcx things.
    movq    %rsp, %rdx              # Copy stack pointer to %rdx. Looks like we're using %rdx instead of %rcx this time.
    movl    $301, %esi              # imm = 0x12D : Copy 301 to %esi.
    movl    %esi, %edi              # Copy %esi to %edi
    movl    %ecx, -7332(%rbp)       # 4-byte Spill : Save %ecx temporarily on stack (holding -literal).
    movq    %rdi, %rcx              # Copy %rdi to %rcx. %rdi now holds 301.
    movq    %rdx, %rdi              # Copy %rdx to %rdi. %rdi now holds stack pointer.
    movq    -7248(%rbp), %rsi       # 8-byte Reload : Copy address of cs (&cs) to %rsi.

    rep;movsq                       # Do the struct copy. Copy of cs now at top of stack.

    leaq    -7240(%rbp), %rdi       # Get a place on the stack to pass as "secret argument" to simplifyLiteral telling
                                    # it where to write its return value to.
    movl    -7332(%rbp), %esi       # 4-byte Reload : Copy variable "-literal" to %esi, 2nd argument reg.
    movq    %rax, -7344(%rbp)       # 8-byte Spill : Copy the address in %rax to the stack.

    callq   simplifyLiteral         # Call simplifyLiteral with cs and -literal. Returned struct stored at -7240(%rbp).

    # Now do the same to pass returned struct by value into dpll function.
    leaq    -7240(%rbp), %rax       # Copy address of returned struct to %rax.
    movq    %rsp, %rcx              # Copy stack pointer to %rcx.
    movl    $301, %esi              # imm = 0x12D : Copy 301 to %esi.
    movl    %esi, %edx              # Copy %esi to %edx.
    leaq    -7240(%rbp), %rsi       # Copy address of returned struct to %rsi to use as source for the copy operation.

    movq    %rcx, -7352(%rbp)       # 8-byte Spill : Store %rcx on stack. 
    movq    %rdx, %rcx              # Copy 301 to %rcx.
    movq    -7352(%rbp), %rdi       # 8-byte Reload : Copy old value of %rcx to %rdi (%rdi now holds stack pointer).

    rep;movsq                       # Do the struct copy. 

    movq    %rax, -7360(%rbp)       # 8-byte Spill : Copy %rax to -7360(%rbp)
                                    # Not sure why, seems like its for safekeeping.

    callq   dpll                    # Call dpll with copy of return value of simplifyLiteral as argument.

    movb    %al, -1(%rbp)           # Copy the char returned by dpll to the stack place where we're keeping things before
                                    # we return from the function.

.LBB3_12:
    movsbl  -1(%rbp), %eax          # Copy the return value in -1(%rbp), whatever it may be, to %eax.
    addq    $9776, %rsp             # imm = 0x2630 : add 9776 bytes back to the stack pointer,
                                    # reducing the size of the stack back to its original size.
    popq    %rbp                    # Pop 64 bits from the stack to restore %rbp to its original value.
    retq                            # Pop return address from stack and jump to it.
.Lfunc_end3:
    .size   dpll, .Lfunc_end3-dpll
    .cfi_endproc

    .type   node_count,@object      # @node_count
    .comm   node_count,8,8
    .type   failures,@object        # @failures
    .comm   failures,8,8
    .type   .L.str,@object          # @.str
    .section    .rodata.str1.1,"aMS",@progbits,1

# These are string literals that get hardcoded into the executable.
# .asciz is ascii with a zero byte terminating the string.
.L.str:
    .asciz  "literal != 0"          # The actual string literal, 
                                    # nothing to do with the fact that it contains the word literal.
    .size   .L.str, 13

    .type   .L.str.1,@object        # @.str.1
.L.str.1:
    .asciz  "dpll.c"
    .size   .L.str.1, 7

    .type   .L__PRETTY_FUNCTION__.dpll,@object # @__PRETTY_FUNCTION__.dpll
.L__PRETTY_FUNCTION__.dpll:
    .asciz  "char dpll(clauseset)"
    .size   .L__PRETTY_FUNCTION__.dpll, 21

    .type   cs_original,@object     # @cs_original
    .comm   cs_original,2408,4

    .ident  "clang version 3.7.0 (tags/RELEASE_370/final)"
    .section    ".note.GNU-stack","",@progbits
