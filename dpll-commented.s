	.text
	.file	"/cs/home/jdh9/.ccache/tmp/dpll.tmp.pc2-017-l.cs.st-andrews.ac.uk.5236.i"
	.globl	containsEmptyClause
	.align	16, 0x90
	.type	containsEmptyClause,@function
containsEmptyClause:                    # @containsEmptyClause, this is the function containsEmptyClause(clauseset* csPtr)
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Ltmp0:
	.cfi_def_cfa_offset 16
.Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp					# Copy contents of register rsp to regist %rbp (what's in rsp at this point?)
	subq	$32, %rsp					# Subtract, q specifies 64 bit argument, subtracts a constant 32 from the value in register rsp.
	movq	%rdi, -16(%rbp)				# Copy contents of register rdi to 16 bits offset from register rbp.
	movq	-16(%rbp), %rdi				# Copy contents of rdi back to rbp, this combined with the previous instruction is kind of pointless.
	callq	numClauses					# Call function numClauses, jumps to numClauses label (in another file?). This is still on the first line of containsEmptyClause()

	movl	%eax, -20(%rbp)				# Copy doubleword in eax to rbp offset by 20 bits.
	movl	$0, -24(%rbp)
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	-24(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jge	.LBB0_6
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	movslq	-24(%rbp), %rax				# rax is for storing a return value
	movq	-16(%rbp), %rcx
	addq	$8, %rcx
	imulq	 $24, %rax
	addq	%rax, %rcx
	cmpl	$0, 4(%rcx)					# This could be the if statement in the for loop.
	jne	.LBB0_4
# BB#3:
	movl	$1, -4(%rbp)
	jmp	.LBB0_7
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	jmp	.LBB0_5
.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
	movl	-24(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -24(%rbp)
	jmp	.LBB0_1
.LBB0_6:
	movl	$0, -4(%rbp)
.LBB0_7:
	movl	-4(%rbp), %eax
	addq	$32, %rsp
	popq	%rbp
	retq
.Ltmp3:
	.size	containsEmptyClause, .Ltmp3-containsEmptyClause
	.cfi_endproc

	.globl	unitClause
	.align	16, 0x90
	.type	unitClause,@function
unitClause:                             # @unitClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Ltmp4:
	.cfi_def_cfa_offset 16
.Ltmp5:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp6:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	callq	numClauses					# Call function numClauses, jumps to numClauses label (in another file?).
	movl	%eax, -20(%rbp)
	movl	$0, -24(%rbp)
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movl	-24(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jge	.LBB1_6
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	movslq	-24(%rbp), %rax
	movq	-16(%rbp), %rcx
	addq	$8, %rcx
	imulq	 $24, %rax
	addq	%rax, %rcx
	cmpl	$1, 4(%rcx)
	jne	.LBB1_4
# BB#3:
	movslq	-24(%rbp), %rax
	movq	-16(%rbp), %rcx
	addq	$8, %rcx
	imulq	 $24, %rax
	addq	%rax, %rcx
	movl	8(%rcx), %edx
	movl	%edx, -4(%rbp)
	jmp	.LBB1_7
.LBB1_4:                                #   in Loop: Header=BB1_1 Depth=1
	jmp	.LBB1_5
.LBB1_5:                                #   in Loop: Header=BB1_1 Depth=1
	movl	-24(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -24(%rbp)
	jmp	.LBB1_1
.LBB1_6:
	movl	$0, -4(%rbp)
.LBB1_7:
	movl	-4(%rbp), %eax
	addq	$32, %rsp
	popq	%rbp
	retq
.Ltmp7:
	.size	unitClause, .Ltmp7-unitClause
	.cfi_endproc

	.globl	firstLiteral
	.align	16, 0x90
	.type	firstLiteral,@function
firstLiteral:                           # @firstLiteral
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Ltmp8:
	.cfi_def_cfa_offset 16
.Ltmp9:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp10:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	callq	numClauses
	movl	%eax, -20(%rbp)
	movl	$0, -24(%rbp)
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movl	-24(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jge	.LBB2_6
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movslq	-24(%rbp), %rax
	movq	-16(%rbp), %rcx
	addq	$8, %rcx
	imulq	 $24, %rax
	addq	%rax, %rcx
	cmpl	$0, 4(%rcx)
	jle	.LBB2_4
# BB#3:
	movslq	-24(%rbp), %rax
	movq	-16(%rbp), %rcx
	addq	$8, %rcx
	imulq	 $24, %rax
	addq	%rax, %rcx
	movl	8(%rcx), %edx
	movl	%edx, -4(%rbp)
	jmp	.LBB2_7
.LBB2_4:                                #   in Loop: Header=BB2_1 Depth=1
	jmp	.LBB2_5
.LBB2_5:                                #   in Loop: Header=BB2_1 Depth=1
	movl	-24(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -24(%rbp)
	jmp	.LBB2_1
.LBB2_6:
	movl	$0, -4(%rbp)
.LBB2_7:
	movl	-4(%rbp), %eax
	addq	$32, %rsp
	popq	%rbp
	retq
.Ltmp11:
	.size	firstLiteral, .Ltmp11-firstLiteral
	.cfi_endproc

	.globl	dpll
	.align	16, 0x90
	.type	dpll,@function
dpll:                                   # @dpll
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Ltmp12:
	.cfi_def_cfa_offset 16
.Ltmp13:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
.Ltmp14:
	.cfi_def_cfa_register %rbp
	subq	$9776, %rsp             # imm = 0x2630
	leaq	16(%rbp), %rax
	movq	node_count, %rcx
	addq	$1, %rcx
	movq	%rcx, node_count
	#APP
	nop

	#NO_APP
	cmpl	$0, 4(%rax)
	movq	%rax, -7248(%rbp)       # 8-byte Spill
	jne	.LBB3_2
# BB#1:
	movb	$1, -1(%rbp)
	jmp	.LBB3_12
.LBB3_2:
	movq	-7248(%rbp), %rdi       # 8-byte Reload
	callq	containsEmptyClause
	cmpl	$0, %eax
	je	.LBB3_4
# BB#3:
	movq	failures, %rax
	addq	$1, %rax
	movq	%rax, failures
	movb	$0, -1(%rbp)
	jmp	.LBB3_12
.LBB3_4:
	movq	-7248(%rbp), %rdi       # 8-byte Reload
	callq	unitClause
	movl	%eax, -8(%rbp)
	cmpl	$0, -8(%rbp)
	je	.LBB3_6
# BB#5:
	leaq	-2416(%rbp), %rax
	movl	-8(%rbp), %esi
	movq	%rsp, %rcx
	movl	$301, %edx              # imm = 0x12D
	movl	%edx, %edi
	movq	%rcx, -7256(%rbp)       # 8-byte Spill
	movq	%rdi, %rcx
	movq	-7256(%rbp), %rdi       # 8-byte Reload
	movq	-7248(%rbp), %r8        # 8-byte Reload
	movl	%esi, -7260(%rbp)       # 4-byte Spill
	movq	%r8, %rsi
	rep;movsq
	leaq	-2416(%rbp), %rdi
	movl	-7260(%rbp), %esi       # 4-byte Reload
	movq	%rax, -7272(%rbp)       # 8-byte Spill
	callq	simplifyLiteral
	leaq	-2416(%rbp), %rax
	movq	%rsp, %rcx
	movl	$301, %edx              # imm = 0x12D
	movl	%edx, %edi
	leaq	-2416(%rbp), %rsi
	movq	%rcx, -7280(%rbp)       # 8-byte Spill
	movq	%rdi, %rcx
	movq	-7280(%rbp), %rdi       # 8-byte Reload
	rep;movsq
	movq	%rax, -7288(%rbp)       # 8-byte Spill
	callq	dpll
	movb	%al, -1(%rbp)
	jmp	.LBB3_12
.LBB3_6:
	movq	-7248(%rbp), %rdi       # 8-byte Reload
	callq	firstLiteral
	movl	%eax, -2420(%rbp)
	cmpl	$0, -2420(%rbp)
	je	.LBB3_8
# BB#7:
	jmp	.LBB3_9
.LBB3_8:
	movabsq	$.L.str, %rdi
	movabsq	$.L.str1, %rsi
	movl	$97, %edx
	movabsq	$.L__PRETTY_FUNCTION__.dpll, %rcx
	callq	__assert_fail
.LBB3_9:
	leaq	-4832(%rbp), %rax
	movl	-2420(%rbp), %esi
	movq	%rsp, %rcx
	movl	$301, %edx              # imm = 0x12D
	movl	%edx, %edi
	movq	%rcx, -7296(%rbp)       # 8-byte Spill
	movq	%rdi, %rcx
	movq	-7296(%rbp), %rdi       # 8-byte Reload
	movq	-7248(%rbp), %r8        # 8-byte Reload
	movl	%esi, -7300(%rbp)       # 4-byte Spill
	movq	%r8, %rsi
	rep;movsq
	leaq	-4832(%rbp), %rdi
	movl	-7300(%rbp), %esi       # 4-byte Reload
	movq	%rax, -7312(%rbp)       # 8-byte Spill
	callq	simplifyLiteral
	leaq	-4832(%rbp), %rax
	movq	%rsp, %rcx
	movl	$301, %edx              # imm = 0x12D
	movl	%edx, %edi
	leaq	-4832(%rbp), %rsi
	movq	%rcx, -7320(%rbp)       # 8-byte Spill
	movq	%rdi, %rcx
	movq	-7320(%rbp), %rdi       # 8-byte Reload
	rep;movsq
	movq	%rax, -7328(%rbp)       # 8-byte Spill
	callq	dpll
	movb	%al, -2421(%rbp)
	cmpb	$0, -2421(%rbp)
	je	.LBB3_11
# BB#10:
	movb	-2421(%rbp), %al
	movb	%al, -1(%rbp)
	jmp	.LBB3_12
.LBB3_11:
	leaq	-7240(%rbp), %rax
	movl	$0, %ecx
	subl	-2420(%rbp), %ecx
	movq	%rsp, %rdx
	movl	$301, %esi              # imm = 0x12D
	movl	%esi, %edi
	movl	%ecx, -7332(%rbp)       # 4-byte Spill
	movq	%rdi, %rcx
	movq	%rdx, %rdi
	movq	-7248(%rbp), %rsi       # 8-byte Reload
	rep;movsq
	leaq	-7240(%rbp), %rdi
	movl	-7332(%rbp), %esi       # 4-byte Reload
	movq	%rax, -7344(%rbp)       # 8-byte Spill
	callq	simplifyLiteral
	leaq	-7240(%rbp), %rax
	movq	%rsp, %rcx
	movl	$301, %esi              # imm = 0x12D
	movl	%esi, %edx
	leaq	-7240(%rbp), %rsi
	movq	%rcx, -7352(%rbp)       # 8-byte Spill
	movq	%rdx, %rcx
	movq	-7352(%rbp), %rdi       # 8-byte Reload
	rep;movsq
	movq	%rax, -7360(%rbp)       # 8-byte Spill
	callq	dpll
	movb	%al, -1(%rbp)
.LBB3_12:
	movsbl	-1(%rbp), %eax
	addq	$9776, %rsp             # imm = 0x2630
	popq	%rbp
	retq
.Ltmp15:
	.size	dpll, .Ltmp15-dpll
	.cfi_endproc

	.type	node_count,@object      # @node_count
	.comm	node_count,8,8
	.type	failures,@object        # @failures
	.comm	failures,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"literal != 0"
	.size	.L.str, 13

	.type	.L.str1,@object         # @.str1
.L.str1:
	.asciz	"dpll.c"
	.size	.L.str1, 7

	.type	.L__PRETTY_FUNCTION__.dpll,@object # @__PRETTY_FUNCTION__.dpll
.L__PRETTY_FUNCTION__.dpll:
	.asciz	"char dpll(clauseset)"
	.size	.L__PRETTY_FUNCTION__.dpll, 21

	.type	cs_original,@object     # @cs_original
	.comm	cs_original,2408,4

	.ident	"clang version 3.5.0 (tags/RELEASE_350/final)"
	.section	".note.GNU-stack","",@progbits
