	.text
	.file	"/cs/home/jdh9/.ccache/tmp/dpll.stdout.trenco.cs.st-andrews.ac.uk.24716.Jeq2YJ.i"
	.globl	containsEmptyClause
	.align	16, 0x90
	.type	containsEmptyClause,@function
containsEmptyClause:
	.cfi_startproc
	pushq	%rbx
.Ltmp0:
	.cfi_def_cfa_offset 16
.Ltmp1:
	.cfi_offset %rbx, -16
	# The theme of the optimised version seems to be that it uses the stack a lot less
	pushq	%rbx 				# Push %rbx to the stack
	movq	%rdi, %rbx 			# Copy argument csPtr to %rbx.
	callq	numClauses 			# Call numClauses with csPtr as first arg in %rdi.
	movl	%eax, %ecx 			# Copies return value (int) to %ecx (local variable "num_clauses")
	xorl	%eax, %eax 			# Zeroes out %eax.
	testl	%ecx, %ecx 			# test does a logical AND, updates the flags for jumping based on the result.
	jle	.LBB0_4 				# Jump if zero flag (ZF) = 1 and sign flag (SF) != 0F
	movslq	%ecx, %rcx 			# Copy num_clauses to %rcx
	addq	$12, %rbx 			# Add 12 to csPtr, this points you right at num_literals in the first clause in csPtr->clauses
	xorl	%edx, %edx 			# Zero out %edx, this is the loop counter?
	.align	16, 0x90 			# Pad out some space with nulls?
.LBB0_3:
	movl	$1, %eax 			# Put 1 in %eax, in case we want to return this right now.
	cmpl	$0, (%rbx) 			# Check if value at address in %rbx is 0.
	je	.LBB0_4 				# If it is, jump to return.
								# otherwise
	incq	%rdx 				# increment %rdx, the loop counter.
	addq	$24, %rbx 			# Move 24 bytes past csPtr, gets you to the num_literals in the next clause.
	xorl	%eax, %eax 			# Zero out return register.
	cmpq	%rcx, %rdx 			# Compare num_clauses and loop counter.
	jl	.LBB0_3 				# If num_clauses < i, keep going? No idea how this is the correct way around.
.LBB0_4:
								# else terminate the loop.
	popq	%rbx 				# Pop %rbx again, restoring it for the caller.
	retq 						# Pop return address and jump there.
.Lfunc_end0:
	.size	containsEmptyClause, .Lfunc_end0-containsEmptyClause
	.cfi_endproc

	.globl	unitClause
	.align	16, 0x90
	.type	unitClause,@function
unitClause:
	.cfi_startproc
	pushq	%rbx 				# Push %rbx to stack.
.Ltmp2:
	.cfi_def_cfa_offset 16
.Ltmp3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx 			# Copy csPtr argument to %rbx
	callq	numClauses 			# Call num clauses with first argument csPtr in %rdi
	movl	%eax, %ecx 			# Copy return value to %ecx (becomes num_clauses).
	xorl	%eax, %eax 			# Zero out return register.
	testl	%ecx, %ecx 			# AND operation on num_clauses with itself.
	jle	.LBB1_5 				# jump if less than, this always false.
	movslq	%ecx, %rcx 			# Copy num_clauses to %rcx.
	leaq	12(%rbx), %rsi 		# 12 bytes points us to num_literals in current clause of clauses array. 
								# Load this address to %rsi.
	xorl	%edx, %edx 			# Zero out %edx, use as loop counter.
	.align	16, 0x90
.LBB1_3:
	cmpl	$1, (%rsi) 			# Compare 1 with csPtr->clauses[i].num_literals
	je	.LBB1_4 				# Jump if equal to return.
	incq	%rdx
	addq	$24, %rsi
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	jl	.LBB1_3
	jmp	.LBB1_5
.LBB1_4:
	# Somehow gets csPtr->clauses[i].num_literals
	leaq	(%rdx,%rdx,2), %rax 
	movl	16(%rbx,%rax,8), %eax
.LBB1_5:
	popq	%rbx
	retq
.Lfunc_end1:
	.size	unitClause, .Lfunc_end1-unitClause
	.cfi_endproc

	.globl	firstLiteral
	.align	16, 0x90
	.type	firstLiteral,@function
firstLiteral:
	.cfi_startproc
	pushq	%rbx
.Ltmp4:
	.cfi_def_cfa_offset 16
.Ltmp5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	numClauses
	movl	%eax, %ecx
	xorl	%eax, %eax
	testl	%ecx, %ecx
	jle	.LBB2_5
	movslq	%ecx, %rcx
	leaq	12(%rbx), %rsi
	xorl	%edx, %edx
	.align	16, 0x90
.LBB2_3:
	cmpl	$0, (%rsi)
	jg	.LBB2_4
	incq	%rdx
	addq	$24, %rsi
	xorl	%eax, %eax
	cmpq	%rcx, %rdx
	jl	.LBB2_3
	jmp	.LBB2_5
.LBB2_4:
	leaq	(%rdx,%rdx,2), %rax
	movl	16(%rbx,%rax,8), %eax
.LBB2_5:
	popq	%rbx
	retq
.Lfunc_end2:
	.size	firstLiteral, .Lfunc_end2-firstLiteral
	.cfi_endproc

	.globl	dpll
	.align	16, 0x90
	.type	dpll,@function
dpll:
	.cfi_startproc
	pushq	%rbp
.Ltmp6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Ltmp7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Ltmp8:
	.cfi_def_cfa_offset 32
	subq	$9632, %rsp
.Ltmp9:
	.cfi_def_cfa_offset 9664
.Ltmp10:
	.cfi_offset %rbx, -32
.Ltmp11:
	.cfi_offset %r14, -24
.Ltmp12:
	.cfi_offset %rbp, -16
	incq	node_count(%rip)
	#APP
	nop

	#NO_APP
	movb	$1, %al
	cmpl	$0, 9668(%rsp)
	je	.LBB3_20
	leaq	9664(%rsp), %r14
	movq	%r14, %rdi
	callq	numClauses
	testl	%eax, %eax
	jle	.LBB3_6
	cltq
	leaq	12(%r14), %rcx
	xorl	%edx, %edx
	.align	16, 0x90
.LBB3_4:
	cmpl	$0, (%rcx)
	je	.LBB3_5
	incq	%rdx
	addq	$24, %rcx
	cmpq	%rax, %rdx
	jl	.LBB3_4
.LBB3_6:
	movq	%r14, %rdi
	callq	numClauses
	testl	%eax, %eax
	jle	.LBB3_12
	cltq
	leaq	12(%r14), %rdx
	xorl	%ecx, %ecx
	.align	16, 0x90
.LBB3_9:
	cmpl	$1, (%rdx)
	je	.LBB3_10
	incq	%rcx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	jl	.LBB3_9
	jmp	.LBB3_12
.LBB3_5:
	incq	failures(%rip)
	xorl	%eax, %eax
	jmp	.LBB3_20
.LBB3_10:
	leaq	(%rcx,%rcx,2), %rax
	movl	16(%r14,%rax,8), %eax
	testl	%eax, %eax
	je	.LBB3_12
	movl	$301, %ecx
	movq	%rsp, %rdi
	movq	%r14, %rsi
	rep;movsq
	leaq	7224(%rsp), %rbx
	movq	%rbx, %rdi
	movl	%eax, %esi
	jmp	.LBB3_19
.LBB3_12:
	movq	%r14, %rdi
	callq	numClauses
	testl	%eax, %eax
	jle	.LBB3_21
	cltq
	leaq	12(%r14), %rdx
	xorl	%ecx, %ecx
	.align	16, 0x90
.LBB3_15:
	cmpl	$0, (%rdx)
	jg	.LBB3_16
	incq	%rcx
	addq	$24, %rdx
	cmpq	%rax, %rcx
	jl	.LBB3_15
	jmp	.LBB3_21
.LBB3_16:
	leaq	(%rcx,%rcx,2), %rax
	movl	16(%r14,%rax,8), %ebp
	testl	%ebp, %ebp
	je	.LBB3_21
	movl	$301, %ecx
	movq	%rsp, %rdi
	movq	%r14, %rsi
	rep;movsq
	leaq	4816(%rsp), %rbx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	simplifyLiteral
	movl	$301, %ecx
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	rep;movsq
	callq	dpll
	testb	%al, %al
	jne	.LBB3_20
	negl	%ebp
	movl	$301, %ecx
	movq	%rsp, %rdi
	movq	%r14, %rsi
	rep;movsq
	leaq	2408(%rsp), %rbx
	movq	%rbx, %rdi
	movl	%ebp, %esi
.LBB3_19:
	callq	simplifyLiteral
	movl	$301, %ecx
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	rep;movsq
	callq	dpll
.LBB3_20:
	movsbl	%al, %eax
	addq	$9632, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB3_21:
	movl	$.L.str, %edi
	movl	$.L.str.1, %esi
	movl	$95, %edx
	movl	$.L__PRETTY_FUNCTION__.dpll, %ecx
	callq	__assert_fail
.Lfunc_end3:
	.size	dpll, .Lfunc_end3-dpll
	.cfi_endproc

	.type	node_count,@object
	.comm	node_count,8,8
	.type	failures,@object
	.comm	failures,8,8
	.type	.L.str,@object
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"literal != 0"
	.size	.L.str, 13

	.type	.L.str.1,@object
.L.str.1:
	.asciz	"dpll.c"
	.size	.L.str.1, 7

	.type	.L__PRETTY_FUNCTION__.dpll,@object
.L__PRETTY_FUNCTION__.dpll:
	.asciz	"char dpll(clauseset)"
	.size	.L__PRETTY_FUNCTION__.dpll, 21

	.type	cs_original,@object
	.comm	cs_original,2408,4

	.ident	"clang version 3.7.0 (tags/RELEASE_370/final)"
	.section	".note.GNU-stack","",@progbits
