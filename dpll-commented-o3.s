	.section	__TEXT,__text,regular,pure_instructions
	.macosx_version_min 10, 11
	.globl	_containsEmptyClause
	.align	4, 0x90
_containsEmptyClause:
	.cfi_startproc
	pushq	%rbp
Ltmp0:
	.cfi_def_cfa_offset 16
Ltmp1:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp2:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	callq	_numClauses
	movl	%eax, -20(%rbp)
	movl	$0, -24(%rbp)
LBB0_1:
	movl	-24(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jge	LBB0_6
	movslq	-24(%rbp), %rax
	movq	-16(%rbp), %rcx
	addq	$8, %rcx
	imulq	$24, %rax, %rax
	addq	%rax, %rcx
	cmpl	$0, 4(%rcx)
	jne	LBB0_4
	movl	$1, -4(%rbp)
	jmp	LBB0_7
LBB0_4:
	jmp	LBB0_5
LBB0_5:
	movl	-24(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -24(%rbp)
	jmp	LBB0_1
LBB0_6:
	movl	$0, -4(%rbp)
LBB0_7:
	movl	-4(%rbp), %eax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_unitClause
	.align	4, 0x90
_unitClause:
	.cfi_startproc
	pushq	%rbp
Ltmp3:
	.cfi_def_cfa_offset 16
Ltmp4:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp5:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	callq	_numClauses
	movl	%eax, -20(%rbp)
	movl	$0, -24(%rbp)
LBB1_1:
	movl	-24(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jge	LBB1_6
	movslq	-24(%rbp), %rax
	movq	-16(%rbp), %rcx
	addq	$8, %rcx
	imulq	$24, %rax, %rax
	addq	%rax, %rcx
	cmpl	$1, 4(%rcx)
	jne	LBB1_4
	movslq	-24(%rbp), %rax
	movq	-16(%rbp), %rcx
	addq	$8, %rcx
	imulq	$24, %rax, %rax
	addq	%rax, %rcx
	movl	8(%rcx), %edx
	movl	%edx, -4(%rbp)
	jmp	LBB1_7
LBB1_4:
	jmp	LBB1_5
LBB1_5:
	movl	-24(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -24(%rbp)
	jmp	LBB1_1
LBB1_6:
	movl	$0, -4(%rbp)
LBB1_7:
	movl	-4(%rbp), %eax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_firstLiteral
	.align	4, 0x90
_firstLiteral:
	.cfi_startproc
	pushq	%rbp
Ltmp6:
	.cfi_def_cfa_offset 16
Ltmp7:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp8:
	.cfi_def_cfa_register %rbp
	subq	$32, %rsp
	movq	%rdi, -16(%rbp)
	movq	-16(%rbp), %rdi
	callq	_numClauses
	movl	%eax, -20(%rbp)
	movl	$0, -24(%rbp)
LBB2_1:
	movl	-24(%rbp), %eax
	cmpl	-20(%rbp), %eax
	jge	LBB2_6
	movslq	-24(%rbp), %rax
	movq	-16(%rbp), %rcx
	addq	$8, %rcx
	imulq	$24, %rax, %rax
	addq	%rax, %rcx
	cmpl	$0, 4(%rcx)
	jle	LBB2_4
	movslq	-24(%rbp), %rax
	movq	-16(%rbp), %rcx
	addq	$8, %rcx
	imulq	$24, %rax, %rax
	addq	%rax, %rcx
	movl	8(%rcx), %edx
	movl	%edx, -4(%rbp)
	jmp	LBB2_7
LBB2_4:
	jmp	LBB2_5
LBB2_5:
	movl	-24(%rbp), %eax
	addl	$1, %eax
	movl	%eax, -24(%rbp)
	jmp	LBB2_1
LBB2_6:
	movl	$0, -4(%rbp)
LBB2_7:
	movl	-4(%rbp), %eax
	addq	$32, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.globl	_dpll
	.align	4, 0x90
_dpll:
	.cfi_startproc
	pushq	%rbp
Ltmp9:
	.cfi_def_cfa_offset 16
Ltmp10:
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
Ltmp11:
	.cfi_def_cfa_register %rbp
	subq	$9776, %rsp
	leaq	16(%rbp), %rax
	movq	_node_count@GOTPCREL(%rip), %rcx
	movq	(%rcx), %rdx
	addq	$1, %rdx
	movq	%rdx, (%rcx)
	## InlineAsm Start
	nop

	## InlineAsm End
	cmpl	$0, 4(%rax)
	movq	%rax, -7248(%rbp)
	jne	LBB3_2
	movb	$1, -1(%rbp)
	jmp	LBB3_12
LBB3_2:
	movq	-7248(%rbp), %rdi
	callq	_containsEmptyClause
	cmpl	$0, %eax
	je	LBB3_4
	movq	_failures@GOTPCREL(%rip), %rax
	movq	(%rax), %rcx
	addq	$1, %rcx
	movq	%rcx, (%rax)
	movb	$0, -1(%rbp)
	jmp	LBB3_12
LBB3_4:
	movq	-7248(%rbp), %rdi
	callq	_unitClause
	movl	%eax, -8(%rbp)
	cmpl	$0, -8(%rbp)
	je	LBB3_6
	leaq	-2416(%rbp), %rax
	movl	-8(%rbp), %esi
	movq	%rsp, %rcx
	movl	$301, %edx
	movl	%edx, %edi
	movq	%rcx, -7256(%rbp)
	movq	%rdi, %rcx
	movq	-7256(%rbp), %rdi
	movq	-7248(%rbp), %r8
	movl	%esi, -7260(%rbp)
	movq	%r8, %rsi
	rep;movsq
	leaq	-2416(%rbp), %rdi
	movl	-7260(%rbp), %esi
	movq	%rax, -7272(%rbp)
	callq	_simplifyLiteral
	leaq	-2416(%rbp), %rax
	movq	%rsp, %rcx
	movl	$301, %edx
	movl	%edx, %edi
	leaq	-2416(%rbp), %rsi
	movq	%rcx, -7280(%rbp)
	movq	%rdi, %rcx
	movq	-7280(%rbp), %rdi
	rep;movsq
	movq	%rax, -7288(%rbp)
	callq	_dpll
	movb	%al, -1(%rbp)
	jmp	LBB3_12
LBB3_6:
	movq	-7248(%rbp), %rdi
	callq	_firstLiteral
	movl	%eax, -2420(%rbp)
	cmpl	$0, -2420(%rbp)
	setne	%cl
	xorb	$1, %cl
	andb	$1, %cl
	movzbl	%cl, %eax
	movslq	%eax, %rdi
	cmpq	$0, %rdi
	je	LBB3_8
	leaq	L___func__.dpll(%rip), %rdi
	leaq	L_.str(%rip), %rsi
	movl	$97, %edx
	leaq	L_.str1(%rip), %rcx
	callq	___assert_rtn
LBB3_8:
	jmp	LBB3_9
LBB3_9:
	leaq	-4832(%rbp), %rax
	movl	-2420(%rbp), %esi
	movq	%rsp, %rcx
	movl	$301, %edx
	movl	%edx, %edi
	movq	%rcx, -7296(%rbp)
	movq	%rdi, %rcx
	movq	-7296(%rbp), %rdi
	movq	-7248(%rbp), %r8
	movl	%esi, -7300(%rbp)
	movq	%r8, %rsi
	rep;movsq
	leaq	-4832(%rbp), %rdi
	movl	-7300(%rbp), %esi
	movq	%rax, -7312(%rbp)
	callq	_simplifyLiteral
	leaq	-4832(%rbp), %rax
	movq	%rsp, %rcx
	movl	$301, %edx
	movl	%edx, %edi
	leaq	-4832(%rbp), %rsi
	movq	%rcx, -7320(%rbp)
	movq	%rdi, %rcx
	movq	-7320(%rbp), %rdi
	rep;movsq
	movq	%rax, -7328(%rbp)
	callq	_dpll
	movb	%al, -2421(%rbp)
	cmpb	$0, -2421(%rbp)
	je	LBB3_11
	movb	-2421(%rbp), %al
	movb	%al, -1(%rbp)
	jmp	LBB3_12
LBB3_11:
	leaq	-7240(%rbp), %rax
	xorl	%ecx, %ecx
	subl	-2420(%rbp), %ecx
	movq	%rsp, %rdx
	movl	$301, %esi
	movl	%esi, %edi
	movl	%ecx, -7332(%rbp)
	movq	%rdi, %rcx
	movq	%rdx, %rdi
	movq	-7248(%rbp), %rsi
	rep;movsq
	leaq	-7240(%rbp), %rdi
	movl	-7332(%rbp), %esi
	movq	%rax, -7344(%rbp)
	callq	_simplifyLiteral
	leaq	-7240(%rbp), %rax
	movq	%rsp, %rcx
	movl	$301, %esi
	movl	%esi, %edx
	leaq	-7240(%rbp), %rsi
	movq	%rcx, -7352(%rbp)
	movq	%rdx, %rcx
	movq	-7352(%rbp), %rdi
	rep;movsq
	movq	%rax, -7360(%rbp)
	callq	_dpll
	movb	%al, -1(%rbp)
LBB3_12:
	movsbl	-1(%rbp), %eax
	addq	$9776, %rsp
	popq	%rbp
	retq
	.cfi_endproc

	.comm	_node_count,8,3
	.comm	_failures,8,3
	.section	__TEXT,__cstring,cstring_literals
L___func__.dpll:
	.asciz	"dpll"

L_.str:
	.asciz	"dpll.c"

L_.str1:
	.asciz	"literal != 0"

	.comm	_cs_original,2408,2

.subsections_via_symbols
